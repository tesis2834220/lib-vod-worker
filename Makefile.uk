# SPDX-License-Identifier: BSD-3-Clause

# Unikraft port of video on demand worker

#
# Authors: Jhon Jairo Sanchez Benavides j.jairosanchez.b@gmail.com 
#
# Copyright (c) 2024, Jhon Jairo Sanchez Benavides. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

################################################################################
# Library registration
################################################################################
$(eval $(call addlib_s,libvodworker,$(CONFIG_LIBVODWORKER)))

################################################################################
# Sources
################################################################################]
LIBVODWORKER_URL = https://gitlab.com/tesis2834220/vod-worker/-/archive/dev/vod-worker-dev.tar.gz
LIBVODWORKER_SUBDIR = vod-worker-dev

$(eval $(call fetch,libvodworker,$(LIBVODWORKER_URL)))

################################################################################
# Helpers
################################################################################
LIBVODWORKER_SRC = $(LIBVODWORKER_ORIGIN)/$(LIBVODWORKER_SUBDIR)

################################################################################
# Library includes
################################################################################
CINCLUDES-y += -I$(LIBVODWORKER_BASE)/include

################################################################################
# Flags
################################################################################
LIBVODWORKER_FLAGS =

# Suppress some warnings to make the build process look neater
LIBVODWORKER_FLAGS_SUPPRESS =

LIBVODWORKER_CFLAGS-y += $(LIBVODWORKER_FLAGS)
LIBVODWORKER_CFLAGS-y += $(LIBVODWORKER_FLAGS_SUPPRESS)

################################################################################
# Glue code
################################################################################
# Include paths
# LIBVODWORKER_CINCLUDES-y   += $(LIBVODWORKER_COMMON_INCLUDES-y)
# LIBVODWORKER_CXXINCLUDES-y += $(LIBVODWORKER_COMMON_INCLUDES-y)


LIBVODWORKER_SRCS-$(CONFIG_LIBVODWORKER_MAIN_FUNCTION) += $(LIBVODWORKER_BASE)/main.c|vodworker


################################################################################
# Library sources
################################################################################
LIBVODWORKER_SRCS-y += $(LIBVODWORKER_SRC)/vodworker.c
LIBVODWORKER_VODWORKER_FLAGS-y += -Dmain=vodworker_main


